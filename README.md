# VLANs

Поднят стенд состоящий из 6 VM
1. testclient1 : 10.10.10.254 (vlan5)
2. testserver1 : 10.10.10.1 (vlan5)
3. testclient2 : 10.10.10.254 (vlan10)
4. testserver2 : 10.10.10.1 (vlan10)
5. centralrouter: 10.10.11.1 (bond0)
6. inetrouter: 10.10.11.2 (bond0)

Все VM подключены в одну общую сеть `internal`

* На testclient1 и testserver1 поднят vlan interface 5
* На testclient2 и testserver2 поднят vlan interface 10
* Настройка выполнена при помощи утилиты vconfig

```
vconfig add eth1 5
ip add add 10.10.10.254/24 dev eth1.5
ip link set eth1.5 up
```
Настройки сохранятся до перезагрузки.

При одинаковых ip адресах они не пересекаются из-за разных vlan. Проверено на каждом интерфейсе связкой tcpdump и ping
# Bonds
* На centralroute и inetrouter поднято по два линка, объединенных в bond на каждой машине.
* При отключении одного из интерфейсов входящих в bond - ping продолжает идти. Если выключить оба интерфейса - bond переходит в down.

Ради интереса, настройка выполнена при помощи конфигурационных файлов интерфейсов располагающихся в /etc/sysconfig/network-scripts/
* Необходимо создать конфигурацию бонда

* `bond cfg`

```
DEVICE=bond0
NAME=bond0
TYPE=Bond
BONDING_MASTER=yes
IPADDR=10.10.11.1
NETMASK=255.255.255.0
ONBOOT=yes
BOOTPROTO=static
BONDING_OPTS="mode=1 miimon=100 fail_over_mac=1"  #mode=1 - режим, в котором будет работать bond (failover balancing etc)
NM_CONTROLLED=no
USERCTL=no
```
* а так же изменить конфигурацию интерфейсов которые будут входить в bond
* `slave cfg`
```
DEVICE=eth1
ONBOOT=yes
BOOTPROTO=none
MASTER=bond0
SLAVE=yes
NM_CONTROLLED=no
USERCTL=no
```

